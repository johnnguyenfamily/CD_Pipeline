CD Pipeline
===========

This is a simple continuous deployment (CD) Jenkins pipeline to deploy the app to docker swarm.
Please refer to the Jenkinsfile for the detail logic.
